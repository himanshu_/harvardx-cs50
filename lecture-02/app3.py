from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def index():
    headline = "Hello Brain!!!"
    return render_template("index3.html",headline=headline)

@app.route("/bye")
def bye():
    headline = "Bubyee!!!"
    return render_template("index3.html",headline=headline)


if __name__=="__main__":
    app.run()
